package sbp.branching;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;
import sbp.common.Utils;
import static org.mockito.ArgumentMatchers.anyString;

public class MyBranchingTests {

        @Test
        @DisplayName("Проверка maxInt")
        void MaxIntTest() {
            int min = 1;
            int max = 5;

            Utils utilsMock = Mockito.mock(Utils.class);
            Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(false);
            Mockito.when(utilsMock.utilFunc2()).thenReturn(false);
            MyBranching myBranching = new MyBranching(utilsMock);

            Assertions.assertEquals(max, myBranching.maxInt(min, max));
            Assertions.assertEquals(max, myBranching.maxInt(max, min));
        }

        @Test
        @DisplayName("Проверка при разных возвращаемых значениях utilFunc2()")
        void ifElseTest() {
            Utils utilsMock = Mockito.mock(Utils.class);
            boolean func2 = true;

            Mockito.when(utilsMock.utilFunc2()).thenReturn(func2);
            MyBranching myBranching = new MyBranching(utilsMock);

            Assertions.assertEquals(func2, myBranching.ifElseExample());

            func2 = false;
            Mockito.when(utilsMock.utilFunc2()).thenReturn(func2);
            Assertions.assertEquals(func2, myBranching.ifElseExample());
        }

        @ParameterizedTest
        @DisplayName("Проверка ветки default")
        @ValueSource(ints = {0, 1, 2, 3})
         void switchTest(int i) {
            Utils utilsMock = Mockito.mock(Utils.class);
            MyBranching myBranching = new MyBranching(utilsMock);
            Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(false);
            if (i == 3) {
                Mockito.when(utilsMock.utilFunc2()).thenReturn(true);
            }else {
                Mockito.when(utilsMock.utilFunc2()).thenReturn(false);
            }
            myBranching.switchExample(i);
            switch (i) {
                case 0:
                case 2:
                    Mockito.verify(utilsMock, Mockito.times(0)).utilFunc1(anyString());
                    Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
                break;

                case 1:
                case 3:
                    Mockito.verify(utilsMock, Mockito.times(1)).utilFunc1(anyString());
                    Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
                break;
            }
        }
    }
